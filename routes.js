const express = require("express");
const mongoose = require("mongoose");
const Profile = require("./models/Profile");
const Address = require("./models/Address");
const router = express.Router();

router.get("/profile", async (req, res) => {
  
  const profiles = await Profile.aggregate([
      {
        $match:{}
      },
      {
        $lookup:{
          from: 'addresses',
          localField: '_id',
          foreignField: 'profileId',
          as: 'addresses'
        }
      }     
  ])
  if(profiles)res.send(profiles);
  else{
    res.status(404);
    res.send({ message: "Profile doesn't exist!" });
  }    
});

router.post("/profile", async (req, res) => {
  console.log(req.body.firstName)
  const profile = new Profile({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    mobileNumber: req.body.mobileNumber,
    birthday: req.body.birthday
  });
  await profile.save().then(profileRes =>{
    res.send(profileRes);
  }).catch(err=>{
    res.status(500);
    res.send({ error: "Database error", message: err.message });
  })
});

router.get("/profile/:id", async (req, res) => {
  try {
    const profile = await Profile.aggregate([
        {
          $match:{ _id: mongoose.Types.ObjectId(req.params.id) }
        },
        {
          $lookup:{
            from: 'addresses',
            localField: '_id',
            foreignField: 'profileId',
            as: 'addresses'
          }
        }     
    ])
    if(profile)res.send(profile);
    else{
      res.status(404);
    res.send({ message: "Profile doesn't exist!" });
    }    
  } catch {
    res.status(404);
    res.send({ error: "Profile doesn't exist!" });
  }
});

router.put("/profile/:id", async (req, res) => {
  try {
    let obj = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      mobileNumber: req.body.mobileNumber,
      birthday: req.body.birthday
    }
    await Profile.updateOne({ _id: req.params.id }, obj);
    res.status(204).send();
  } catch {
    res.status(404);
    res.send({ error: "Profile doesn't exist!" });
  }
});

router.delete("/profile/:id", async (req, res) => {
  try {
    await Profile.deleteOne({ _id: req.params.id });
    res.status(204).send();
  } catch {
    res.status(404);
    res.send({ error: "Profile doesn't exist!" });
  }
});

/** Address routes */

router.get("/address", async (req, res) => {  
  const address = await Address.find()
  res.send(address);
});

router.post("/address", async (req, res) => {
  const address = new Address({
    street: req.body.street,
    city: req.body.city,
    pinCode: req.body.pinCode,
    type: req.body.type,
    profileId: req.body.profileId
  });
  await address.save().then(addressRes =>{
    res.send(addressRes);
  }).catch(err=>{
    res.status(500);
    res.send({ error: "Database error", message: err.message });
  })
});

router.get("/address/:id", async (req, res) => {
  try {
    const address = await Address.findOne({ _id: req.params.id });
    if(address)res.send(address);
    else{
      res.status(404);
      res.send({ error: "Address doesn't exist!" });
    }
  } catch {
    res.status(404);
    res.send({ error: "Address doesn't exist!" });
  }
});

router.put("/address/:id", async (req, res) => {
  try {
    let obj = {
      street: req.body.street,
      city: req.body.city,
      pinCode: req.body.pinCode,
      type: req.body.type,
    }
    await Address.updateOne({ _id: req.params.id }, obj);
    res.status(204).send();
  } catch {
    res.status(404);
    res.send({ error: "Address doesn't exist!" });
  }
});

router.delete("/address/:id", async (req, res) => {
  try {
    await Address.deleteOne({ _id: req.params.id });
    res.status(204).send();
  } catch {
    res.status(404);
    res.send({ error: "Address doesn't exist!" });
  }
});

module.exports = router;
