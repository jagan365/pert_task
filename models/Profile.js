const mongoose = require("mongoose");
const { Schema } = mongoose;

const schema = new Schema({
  firstName: {type: String, require: true },
  lastName: {type: String, require: true },
  mobileNumber: {type: Number, require: true },
  birthday: {type: Date}
},
{ timestamps: true });

module.exports = mongoose.model("Profiles", schema);
