const mongoose = require("mongoose");

const schema = mongoose.Schema({
  street: {type: String, require: true },
  city: {type: String, require: true },
  pinCode: {type: Number, require: true },
  type: {type: String, require: true },
  profileId: {
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'Profiles'
  }
},{ timestamps: true });

module.exports = mongoose.model("Addresses", schema);
